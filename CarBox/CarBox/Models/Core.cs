﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CarBox.Models
{
    public class Core
    {
        public static string GetHash(string source)
        {
            byte[] hash = null;
            var data = Encoding.UTF8.GetBytes(source);
            using (SHA512 shaM = new SHA512Managed())
            {
                hash = shaM.ComputeHash(data);
            }
            return BitConverter.ToString(hash).Replace("-", "");
        }
    }
}
