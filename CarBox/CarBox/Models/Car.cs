﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarBox.Models
{
    public class Car
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Не указана марка")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Mark { get; set; }
        [Required(ErrorMessage = "Не указан номер")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Number { get; set; }
        [Required(ErrorMessage = "Не указан цвет")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Color { get; set; }
        [Required(ErrorMessage = "Не указана модель")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Model { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
