﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarBox.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Не указан Email")]
        [EmailAddress(ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Не указано имя")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Не указана фамилия")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Не указан пароль")]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Длина пароля не должна быть длинне 255 символов")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Не указан адрес")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Не указан дата")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Не указан телефон")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Phone { get; set; }
        public ICollection<Car> Cars { get; set; }
        public int? RoleId { get; set; }
        public Role Role { get; set; }
        public Guid? restorId { get; set; }
    }
}
