﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using static CarBox.Models.Core;

namespace CarBox.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string userRoleName = "user";

            string adminEmail = "rosk@mail.ru";
            string adminFirstName = "Denis";
            string adminLastName = "Roskidniy";
            string adminPassword = GetHash("1234");
            string adminAddress = "Svoboda, 26, 18";
            DateTime adminDate = new DateTime(1983, 05, 16);
            string adminPhone = "89281349019";

            Role adminRole = new Role { Id = 1, Name = adminRoleName };
            Role userRole = new Role { Id = 2, Name = userRoleName };
            User adminUser = new User { Id = 1, Email = adminEmail, FirstName = adminFirstName, LastName = adminLastName, Password = adminPassword, Address = adminAddress, Date = adminDate, Phone = adminPhone, RoleId = adminRole.Id };

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });
            base.OnModelCreating(modelBuilder);
        }
    }
}
