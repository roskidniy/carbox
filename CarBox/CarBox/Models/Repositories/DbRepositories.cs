﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using static CarBox.Models.Core;


namespace CarBox.Models.Repositories
{
    public class DbRepositories<T> : IDbRepositories<T> where T : class
    {
        DbSet<T> dbSet;
        
        private ApplicationContext db;

        public DbRepositories(ApplicationContext context)
        {
            db = context;
            dbSet = context.Set<T>();
        }
        public void Create(T item)
        {
            dbSet.Add(item);
            db.SaveChanges();
        }

        public void Delete(int? id)
        {
            T item = dbSet.Find(id);
            if (item != null)
                dbSet.Remove(item);
                db.SaveChanges();
        }
        
        public T Get(int? id)
        {
            return dbSet.Find(id);
        }

        public IEnumerable<T> GetList()
        {
            return dbSet.ToList();
        }

        public Role GetName(string name)
        {
            return db.Roles.FirstOrDefault(u => u.Name == "user");
        }

        public string GetRole(Claim identityClaim)
        {
            return db.Users.Include(r => r.Role)
                           .FirstOrDefault(u => u.Email == identityClaim.Value).Role.Name;
        }

        public User GetUserByEmail(string email)
        {
            return db.Users.FirstOrDefault(u => u.Email == email);
        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            return db.Users.Include(u => u.Role)
                    .FirstOrDefault(u => u.Email == email && u.Password == GetHash(password));
        }

        public User GetUserByRestoreId(Guid? id)
        {
            return db.Users.FirstOrDefault(u => u.restorId == id);
        }

        public void Update(T item)
        {
            dbSet.Update(item);
            db.SaveChanges();
        }

        public void UpdateAndRestorIdNull(User user)
        {
            db.Users.Update(user);
            user.restorId = null;
            db.SaveChanges();
        }
    }
}
