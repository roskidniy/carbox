﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CarBox.Models.Repositories
{
    public interface IDbRepositories<T>
        where T : class
    {
        IEnumerable<T> GetList();
        T Get(int? id); 
        void Create(T item); 
        void Update(T item); 
        void Delete(int? id);
        string GetRole(Claim identityClaim);
        User GetUserByEmailAndPassword(string email, string password);
        User GetUserByEmail(string email);
        Role GetName(string name);
        User GetUserByRestoreId(Guid? id);
        void UpdateAndRestorIdNull(User user);
    }
}
