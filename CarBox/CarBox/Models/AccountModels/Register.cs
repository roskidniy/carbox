﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarBox.Models.AccountModels
{
    public class Register
    {
        [Required(ErrorMessage = "Не указан Email")]
        [EmailAddress(ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Не указано имя")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Не указана фамилия")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль не совпадает")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Не указан адрес")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Не указано дата")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Не указано телефон")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Длина имени не должна быть длиннее 40 символов")]
        public string Phone { get; set; }
    }
}
