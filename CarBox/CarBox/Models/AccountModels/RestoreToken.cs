﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarBox.Models.AccountModels
{
    public class RestoreToken
    {
        public Guid? Id { get; set; }
    }
}
