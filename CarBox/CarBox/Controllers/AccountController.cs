﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CarBox.Models;
using CarBox.Models.AccountModels;
using CarBox.Models.Class;
using CarBox.Models.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static CarBox.Models.Core; 

namespace CarBox.Controllers
{
    public class AccountController : Controller
    {
        IDbRepositories<User> rep;
        IHttpContextAccessor _httpContextAccessor;

        public AccountController(IDbRepositories<User> urep, IHttpContextAccessor httpContextAccessor)
        {
            rep = urep;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Login model)
        {
            if (ModelState.IsValid)
            {
                User user = rep.GetUserByEmailAndPassword(model.Email, model.Password);
                if(user != null)
                {
                    await Authenticate(user);
                    return RedirectToAction("Index", "Users");
                }
                ModelState.AddModelError("Password", "Неправильно введен адрес или пароль");
            }
            return View();
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        [HttpGet]
        public IActionResult RecoveryPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RecoveryPassword(Recovery model)
        {
            if (ModelState.IsValid)
            {
                User user = rep.GetUserByEmail(model.Email);
                if(user != null && user.Email == model.Email)
                {
                    string hostUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    Guid restoreId = Guid.NewGuid();
                    string path = $"{hostUrl}/Account/Restore/{restoreId.ToString()}";
                    EmailService emailService = new EmailService();
                    await emailService.SendEmailAsync(model.Email, "Восстановление пароля", "Добрый вечер" + " " + user.FirstName + " " + user.LastName + "! Мы получили от вас запрос на восстановление пароля. Если вы действительно хотите восстановить пароль, то перейдите по < a href = " + path + " > ссылке </ a > ");
                    user.restorId = restoreId;
                    rep.Update(user);
                }
                return Redirect("Message");
            }
            return Redirect("RecoveryPassword");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(Register model)
        {
            if (ModelState.IsValid)
            {
                User user = rep.GetUserByEmail(model.Email);
                    if(user == null)
                {
                    Role userRole = rep.GetName(""); 
                    var regUser = new User { Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, Password = GetHash(model.Password), Address = model.Address, Date = model.Date, Phone = model.Phone, Role = userRole };
                    rep.Create(regUser);
                    await Authenticate(regUser);
                    return RedirectToAction("Index", "Users");
                }
                ModelState.AddModelError("Password", "Проверьте данные");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Restore(RestoreToken model)
        {
            User user = rep.GetUserByRestoreId(model.Id);
            if (user != null)
            {
                var newPasswordModel = new NewPassword()
                {
                    RestoreId = user.restorId
                };
                return RedirectToAction("NewPassword", newPasswordModel);
            }
            else
                return Redirect("Failed");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetNewPassword(NewPassword model)
        {
            if (ModelState.IsValid)
            {
                User user = rep.GetUserByRestoreId(model.RestoreId);
                if (model.Password == model.ConfirmNewPassword)
                {
                    user.Password = GetHash(model.Password);
                    rep.UpdateAndRestorIdNull(user);
                }
                else
                    ModelState.AddModelError("Password", "Некорректный пароль");
            }
            return View("Login");
        }

        [HttpGet]
        public IActionResult Failed()
        {
            return View();
        }

        [HttpGet]
        public IActionResult NewPassword(NewPassword model)
        {
            return View(model);
        }

        [HttpGet]
        public IActionResult Message()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Account()
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);

            var user = rep.GetUserByEmail(identityClaim.Value);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Account(User user)
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);

            var userAc = rep.GetUserByEmail(identityClaim.Value);
            {
                userAc.Email = user.Email;
                userAc.FirstName = user.FirstName;
                userAc.LastName = user.LastName;
                userAc.Address = user.Address;
                userAc.Date = user.Date;
                userAc.Phone = user.Phone;
                userAc.Role = user.Role;
            }
            rep.Update(userAc);
            return RedirectToAction("Index", "Users");
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(Change model)
        {
            if (ModelState.IsValid)
            {
                if (model.NewPassword == model.ConfirmNewPassword)
                {
                    var identity = User.Identity as ClaimsIdentity;
                    Claim identityClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);

                    var userAc = rep.GetUserByEmail(identityClaim.Value);

                    var oldPassword = GetHash(model.Password);
                    if (oldPassword == userAc.Password)
                    {
                        userAc.Password = GetHash(model.NewPassword);
                        rep.Update(userAc);
                        return RedirectToAction("Index", "Users");
                    }
                    else
                        ModelState.AddModelError("Password", "Неправильно ввели пароль");
                }
                else
                    ModelState.AddModelError("ConfirmNewPassword", "Пароли не совпадают");
            }
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}