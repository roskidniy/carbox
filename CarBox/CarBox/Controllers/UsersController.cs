﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CarBox.Models;
using CarBox.Models.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarBox.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        IDbRepositories<User> repo;
        public UsersController(IDbRepositories<User> urepo)
        {
            repo = urepo;
        }
        public IActionResult Index()
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
            ViewBag.Role = repo.GetRole(identityClaim);
            return View(repo.GetList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                repo.Create(user);
                return RedirectToAction("Index");
            }
            else
                return Create();
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id != null)
            {
                User user = repo.Get(id);
                if (user != null)
                    return View(user);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                repo.Update(user);
                return RedirectToAction("Index");
            }
            else
                return Edit(user);
        }
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(int? id)
        {
            if (id != null)
            {
                User user = repo.Get(id);
                if (user != null)
                    return View(user);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Delete(int? id)
        {
            if (id != null)
            {
                repo.Delete(id);
                    return RedirectToAction("Index");
                
            }
            return NotFound();
        }
    }
}