﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarBox.Models;
using CarBox.Models.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CarBox.Controllers
{
    [Authorize]
    public class CarsController : Controller
    {
        IDbRepositories<Car> repo;
        IDbRepositories<User> rep;
        public CarsController(IDbRepositories<Car> crepo, IDbRepositories<User> urep)
        {
            repo = crepo;
            rep = urep;
        }
        public IActionResult Index()
        {
            return View(repo.GetList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            SelectList users = new SelectList(rep.GetList(), "Id", "FirstName");
            ViewBag.Users = users;
            return View();
        }

        [HttpPost]
        public IActionResult Create(Car car)
        {
            repo.Create(car);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id != null)
            {
                SelectList users = new SelectList(rep.GetList(), "Id", "FirstName");
                ViewBag.Users = users;
                Car car = repo.Get(id);
                if (car != null)
                    return View(car);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Edit(Car car)
        {
            repo.Update(car);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Car car = repo.Get(id);
                if (car != null)
                    return View(car);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Delete(int? id)
        {
            if (id != null)
            {
                repo.Delete(id);
                    return RedirectToAction("Index");
                
            }
            return NotFound();
        }
    }
}